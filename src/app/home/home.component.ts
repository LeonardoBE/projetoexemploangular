import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-login></app-login>
    <app-login></app-login>
    <app-login></app-login>
    <app-login></app-login>
    <pre>
    {{people | json}}
    <pre>
    <div>
      <table>
      <th>
        NOME
      </th>
      <th>
        GENDER
      </th>
        <tbody>
          <tr *ngFor="let person of people">
            <td>
              {{ person.name }}
            </td>
            <td>
              {{ person.gender}}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  `,
  styles: [

  ]
})
export class HomeComponent {
  public people: any[] = [
    {
      "name": "Luke Skywalker",
      "height": "172",
      "mass": "77",
      "hair_color": "blond",
      "skin_color": "fair",
      "eye_color": "blue",
      "birth_year": "19BBY",
      "gender": "male",
    },
    {
      "name": "Luke Skywalker 222",
      "height": "172",
      "mass": "77",
      "hair_color": "blond",
      "skin_color": "fair",
      "eye_color": "blue",
      "birth_year": "19BBY",
      "gender": "male|male",
    },
  ];
}
